# Open Source/Hardware Matter Based Heating/Cooling Manager

Different Elements are Split into different subprojects of this group

- Main Thermal Controller Software
- Matter/Thread remote temperature/envionment Sensor PCB
- Matter/Thread remote temperature/envionment Sensor Firmware
- [Sequentmicrosystems RPI Automation HAT Linux Kernel Driver](https://gitlab.com/mater-heating-manager/sequentmicrosystems-rpi-automation-hat-linux-kernel-driver)
- Matter/Thread Remote Relay Controller PCB
- Matter/Thread Remote Relay Controller Firmware

For information on Matter visit https://csa-iot.org/all-solutions/matter/




